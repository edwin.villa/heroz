package io.github.edwvilla.heroz.ui.hero_detail

import android.util.Log
import androidx.compose.foundation.*
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberImagePainter
import io.github.edwvilla.heroz.data.models.*


@Composable
fun HeroDetailScreen() {
    val viewModel: HeroDetailViewModel = viewModel()
    val hero = viewModel.heroState.value

    val scrollState = rememberScrollState()

    if (hero == null) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(MaterialTheme.colors.background),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            CircularProgressIndicator(
                modifier = Modifier.size(50.dp)
            )
        }
    }
    else {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .verticalScroll(scrollState)
                .background(MaterialTheme.colors.background),
        ) {
            Image(
                painter = rememberImagePainter(data = hero.image.url),
                contentDescription = null,
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .requiredHeight(400.dp)
            )
            Text(
                text = hero.name,
                style = MaterialTheme.typography.h4.copy(
                    fontWeight = FontWeight.Bold,
                    color = Color.White,
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 30.dp, vertical = 10.dp)
            )
            PowerStatsComponent(hero.powerstats)
            BiographyComponent(hero.biography)
            AppearanceComponent(hero.appearance)
            WorkComponent(hero.work)
            ConnectionsComponent(hero.connections)
        }
    }
}

@Composable
fun BiographyComponent(biography: Biography){
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 30.dp, vertical = 10.dp)
    ) {
        Column(
            modifier = Modifier.padding(10.dp)
        ) {
            Text(
                text = "Biography",
                style = MaterialTheme.typography.h6,
                modifier = Modifier.padding(bottom = 5.dp)
            )
            BiographyTile(title = "Full name", label = biography.fullName)
            BiographyTile(title = "Alter egos", label = biography.alterEgos)
            Text("Aliases: ",
                style = MaterialTheme.typography.subtitle1.copy(
                    fontWeight = FontWeight.Bold
                ),
                modifier = Modifier.padding(vertical = 5.dp)
            )
            biography.aliases.map {
                alias ->
                Text(
                    alias,
                    modifier = Modifier.padding(vertical = 5.dp)
                )
            }
            Text("Place of birth: ",
                style = MaterialTheme.typography.subtitle1.copy(
                    fontWeight = FontWeight.Bold
                ),
                modifier = Modifier.padding(vertical = 5.dp)
            )
            Text(
                biography.firstAppearance,
                modifier = Modifier.padding(vertical = 5.dp)
            )
            Text("Publisher: ",
                style = MaterialTheme.typography.subtitle1.copy(
                    fontWeight = FontWeight.Bold
                ),
                modifier = Modifier.padding(vertical = 5.dp)
            )
            Text(
                biography.publisher,
                modifier = Modifier.padding(vertical = 5.dp)
            )
            BiographyTile(title = "Alignment", label = biography.alignment)
        }
    }
}

@Composable
fun AppearanceComponent(appearance: Appearance){

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 30.dp, vertical = 10.dp)
    ) {
        Column(
            modifier = Modifier.padding(10.dp)
        ) {
           Text(
                text = "Appearance",
                style = MaterialTheme.typography.h6,
                modifier = Modifier.padding(bottom = 5.dp)
            )
            BiographyTile(title = "Gender", label = appearance.gender)
            BiographyTile(title = "Race", label = appearance.race)
            BiographyTile(title = "Height", label = "${appearance.height.first()} / ${appearance.height.last()}")
            BiographyTile(title = "Weight", label = "${appearance.weight.first()} / ${appearance.weight.last()}")
            BiographyTile(title = "Eye color", label = appearance.eyeColor)
            BiographyTile(title = "Hair color", label = appearance.hairColor)
        }
    }
}

@Composable
fun BiographyTile(title: String, label: String) {
    Row(
        verticalAlignment = Alignment.Bottom,
        modifier = Modifier.padding(vertical = 5.dp)
    ) {
       Text("$title: ",
           style = MaterialTheme.typography.subtitle1.copy(
               fontWeight = FontWeight.Bold
           ),
       )
        Text(label)
    }
}

@Composable
 fun WorkComponent(work: Work){
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 30.dp, vertical = 10.dp)
    ) {
        Column(
            modifier = Modifier.padding(10.dp)
        ) {
            Text(
                text = "Work",
                style = MaterialTheme.typography.h6,
                modifier = Modifier.padding(bottom = 5.dp)
            )
            Text("Occupation: ",
                style = MaterialTheme.typography.subtitle1.copy(
                    fontWeight = FontWeight.Bold
                ),
                modifier = Modifier.padding(vertical = 5.dp)
            )
            Text(
                work.occupation,
                modifier = Modifier.padding(vertical = 5.dp)
            )
            Text("Base: ",
                style = MaterialTheme.typography.subtitle1.copy(
                    fontWeight = FontWeight.Bold
                ),
                modifier = Modifier.padding(vertical = 5.dp)
            )
            Text(
                work.base,
                modifier = Modifier.padding(vertical = 5.dp)
            )
        }
    }
 }

@Composable
fun ConnectionsComponent(connections: Connections){
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 30.dp, vertical = 10.dp)
    ) {
        Column(
            modifier = Modifier.padding(10.dp)
        ) {
            Text(
                text = "Connections",
                style = MaterialTheme.typography.h6,
                modifier = Modifier.padding(bottom = 5.dp)
            )
            Text("Group affiliation: ",
                style = MaterialTheme.typography.subtitle1.copy(
                    fontWeight = FontWeight.Bold
                ),
                modifier = Modifier.padding(vertical = 5.dp)
            )
            Text(
                connections.groupAffiliation,
                modifier = Modifier.padding(vertical = 5.dp)
            )
            Text("Relatives: ",
                style = MaterialTheme.typography.subtitle1.copy(
                    fontWeight = FontWeight.Bold
                ),
                modifier = Modifier.padding(vertical = 5.dp)
            )
            Text(
                connections.relatives,
                modifier = Modifier.padding(vertical = 5.dp)
            )
        }
    }
}

@Composable
fun PowerStatsComponent(stats: Powerstats){
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 30.dp, vertical = 10.dp)
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp)
        ) {
            Text(
                text = "Power stats",
                style = MaterialTheme.typography.h6,
                modifier = Modifier.padding(bottom = 5.dp)
            )
            PowerStatsItem("Intelligence", stats.intelligence)
            PowerStatsItem("Strength", stats.strength)
            PowerStatsItem("Speed", stats.speed)
            PowerStatsItem("Durability", stats.durability)
            PowerStatsItem("Power", stats.power)
            PowerStatsItem("Combat", stats.combat)
        }
    }
}

@Composable
fun PowerStatsItem(name: String, percentage: String) {
    Column(
        modifier = Modifier.fillMaxWidth()
    ){
        Row(
            modifier = Modifier
                .padding(bottom = 5.dp, top = 10.dp)
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = name,
                style = MaterialTheme.typography.subtitle1,
            )
            Text(
                text = "${if (percentage == "null") 0 else percentage} / 100",
                style = MaterialTheme.typography.subtitle2
            )
        }

        LinearProgressIndicator(
            progress = if (percentage == "null") {
                0/100f
            } else {
                percentage.toInt() / 100f
            },
            modifier = Modifier
                .fillMaxWidth()
                .height(8.dp)

        )
    }
}