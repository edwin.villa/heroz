package io.github.edwvilla.heroz.ui.theme

import androidx.compose.ui.graphics.Color


val grey = Color(0xFF393E46)
val darkerGrey = Color(0xFF232931)
val Teal = Color(0xFF4ECCA3)
val TealVariant = Color(0xD3AEEBE6)
val acidGreen = Color(0xFF8FFE09)