package io.github.edwvilla.heroz.ui.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.runtime.Composable

private val DarkColorPalette = darkColors(
        primary = Teal,
        primaryVariant = TealVariant,
        secondary = acidGreen,
        background = darkerGrey,
        surface = grey
)

@Composable
fun HerozTheme(content: @Composable() () -> Unit) {
    MaterialTheme(
            colors = DarkColorPalette,
            typography = Typography,
            shapes = Shapes,
            content = content
    )
}