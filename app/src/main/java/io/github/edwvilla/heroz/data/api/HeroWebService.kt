package io.github.edwvilla.heroz.data.api

import android.util.Log
import io.github.edwvilla.heroz.data.models.Hero
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path


class HeroWebService {
    private var api: HeroApi
    private val accessToken: String = "4209839629138907"

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://superheroapi.com/api/$accessToken/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

         api = retrofit.create(HeroApi::class.java)
    }

    suspend fun getHero(id: String): Hero {
        return api.getHero(id)
    }

    interface HeroApi {
        @GET("{id}")
        suspend fun getHero(@Path("id") id: String): Hero
    }
}