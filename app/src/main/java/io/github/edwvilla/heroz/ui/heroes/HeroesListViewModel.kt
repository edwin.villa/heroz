package io.github.edwvilla.heroz.ui.heroes

import android.util.Log
import androidx.compose.runtime.*
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.github.edwvilla.heroz.data.models.Hero
import io.github.edwvilla.heroz.data.models.HeroesRepository
import io.github.edwvilla.heroz.data.models.PAGE_SIZE
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HeroesListViewModel(private val repository: HeroesRepository = HeroesRepository()): ViewModel() {
    init {
        viewModelScope.launch (Dispatchers.IO) {
                val heroes = getHeroes()
            heroesState.value = heroes
        }
    }

    val heroesState: MutableState<List<Hero>> = mutableStateOf(emptyList<Hero>())

    private suspend fun getHeroes(): List<Hero> {
        return repository.getHeroes()
    }

    // infinite scroll implementation
    val page = mutableStateOf(1)
    val loading = mutableStateOf(false)

    private var heroListScrollPosition = 0

    private fun incrementPage(){
        page.value = page.value + 1
    }

    private fun appendHeroes(heroes: List<Hero>){
        val current = ArrayList(heroesState.value)
        current.addAll(heroes)
        heroesState.value = current
    }

    fun onChangePosition(position: Int) {
        heroListScrollPosition = position

        // fetch and prevent duplicates
        if((position + 1) >= (page.value * PAGE_SIZE) && !loading.value) {
            getMoreHeroes()
        }
    }

    fun getMoreHeroes() {
        viewModelScope.launch (Dispatchers.IO) {
            loading.value = true
            incrementPage()
            Log.d("getMoreHeroes", "fetching heroes on page ${page.value}")
            if (page.value > 1) {
                val res = repository.getHeroes(page.value)
                appendHeroes(res)
            }
            loading.value = false
        }
    }


}