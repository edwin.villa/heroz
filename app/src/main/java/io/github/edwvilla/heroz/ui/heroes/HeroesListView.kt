package io.github.edwvilla.heroz.ui.heroes

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberImagePainter
import io.github.edwvilla.heroz.data.models.Hero
import io.github.edwvilla.heroz.data.models.PAGE_SIZE

@Composable
 fun HeroesListScreen(navCallback: (String) -> Unit) {
    val viewModel: HeroesListViewModel = viewModel()
    val heroes = viewModel.heroesState.value
    val page = viewModel.page.value
    val loading = viewModel.loading.value

    if (heroes.isEmpty()) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(MaterialTheme.colors.background),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            CircularProgressIndicator(
                modifier = Modifier.size(50.dp)
            )
        }
    }
    else {
        LazyColumn(
            contentPadding = PaddingValues(16.dp),
            modifier = Modifier.background(MaterialTheme.colors.background)
        ) {
            item {
                Text("Heroes and villains",
                    style = MaterialTheme.typography.h5.copy(
                        color = Color.White,
                        fontWeight = FontWeight.Bold
                    ),
                    modifier = Modifier.padding(vertical = 10.dp)
                )
            }
            itemsIndexed(heroes) {
                index, hero ->
                viewModel.onChangePosition(index)
                HeroListTile(hero, navCallback)
            }

            if (loading)
                item {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(vertical = 30.dp),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.Center,
                    ) {
                        CircularProgressIndicator(
                            modifier = Modifier.size(50.dp)
                        )
                    }
                }
        }
    }
}

@Composable
fun HeroListTile(hero: Hero, navCallback: (String) -> Unit) {
    Card(
        shape = RoundedCornerShape(8.dp),
        elevation = 2.dp,
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 16.dp)
            .clickable {
                navCallback(hero.id)
            }
    ) {
        Row {
            Image(
                painter = rememberImagePainter(
                    data = hero.image.url
                ),
                contentDescription = null,
                modifier = Modifier
                    .size(
                        width = 80.dp,
                        height = 100.dp
                    )
                    .padding(4.dp)
                    .clip(RoundedCornerShape(8.dp))
            )
            Column(
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .padding(16.dp)
            ) {
                Text(
                    text = hero.name,
                    style = MaterialTheme.typography.h6
                )
                Text(
                    text = hero.biography.publisher,
                    style = MaterialTheme.typography.subtitle1
                )
            }
        }
    }


}
