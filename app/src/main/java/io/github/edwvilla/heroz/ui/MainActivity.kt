package io.github.edwvilla.heroz.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navArgument
import androidx.navigation.compose.rememberNavController
import io.github.edwvilla.heroz.ui.hero_detail.HeroDetailScreen

import io.github.edwvilla.heroz.ui.heroes.HeroesListScreen
import io.github.edwvilla.heroz.ui.theme.HerozTheme
import kotlin.reflect.typeOf

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            HerozTheme {
                HerozApp()
            }
        }
    }
}

@Composable
private fun HerozApp(){
    val navController = rememberNavController()
    
    NavHost(
        navController = navController,
        startDestination = "destination_heroes_list",
    ){
        composable(route = "destination_heroes_list"){
            HeroesListScreen() { heroId ->
                navController.navigate("destination_heroes_details/$heroId")
            }
        }
        composable(
            route = "destination_heroes_details/{hero_id}",
            arguments = listOf(
                navArgument("hero_id"){
                    type = NavType.StringType
                }
            )
        ){
            HeroDetailScreen()
        }
    }
}