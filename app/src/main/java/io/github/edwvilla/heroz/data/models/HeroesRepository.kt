package io.github.edwvilla.heroz.data.models

import io.github.edwvilla.heroz.data.api.HeroWebService

const val PAGE_SIZE = 10

class HeroesRepository(private val webService: HeroWebService = HeroWebService()) {

    suspend fun getHero(id: String): Hero {
        return webService.getHero(id)
    }

    // did not find getAll method in api documentation, simulating 10 heroes
    suspend fun getHeroes(page: Int = 1): List<Hero> {
        val list: MutableList<Hero> = mutableListOf()

        val initial = page + ((page - 1) * PAGE_SIZE)
        val final = initial + PAGE_SIZE

        for (index: Int in initial until final) {
            list.add(
                webService.getHero(index.toString())
            )
        }

        return list
    }
}