package io.github.edwvilla.heroz.ui.hero_detail

import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.github.edwvilla.heroz.data.models.Hero
import io.github.edwvilla.heroz.data.models.HeroesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HeroDetailViewModel(
    private val savedStateHandle: SavedStateHandle,
    ): ViewModel() {
    var heroState = mutableStateOf<Hero?>(null)
    private val repository: HeroesRepository = HeroesRepository()

    init {
        val heroId = savedStateHandle.get<String>("hero_id") ?: ""

        viewModelScope.launch (Dispatchers.IO) {
            val hero = repository.getHero(heroId)
            heroState.value = hero
        }
    }
}